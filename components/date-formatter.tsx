import { parseISO, format } from 'date-fns'
import React from "react";

type Props = {
  dateString: string
}

const DateFormatter = ({ dateString }: Props) => <time
    dateTime={dateString}>{format(parseISO(dateString), 'LLLL	d, yyyy')}</time>

export default DateFormatter
