---
title: 'Getting started with the Raspberry pi GPIO with Python'
excerpt: ''
coverImage: '/assets/blog/article-001/raspberry-pi.webp'
date: '2020-11-16T05:35:07.322Z'
author:
  name: Kasper J. Hermansen
  picture: '/assets/blog/authors/jj.jpeg'
ogImage:
  url: '/assets/blog/article-001/raspberry-pi.webp'
---

# Programming the raspberry pi GPIO with Python

## Introduction

The raspberry pi is an awesome device. It is a powerful computer for a small device, while still having easy to use hardware access. In this tutorial, we will explore what is required for programming the Raspberry Pi with Python, and as a result get a small example of how to both read and write to some hardware devices, in this case just a simple circuit.

## Prerequisites

- Raspberry Pi (We will use a raspberry pi 4B, but you can use which ever you want)
- Breadboard (Optional, but recommended)
- Resistor
  - 1k Ohm
  - 300 Ohm
- Button
- Diode

## Installation

Your raspberry pi should already have python3 installed, if it doesn't, simply install it from the official repositories.

`sudo apt-get update && sudo apt install python3`

After that enter a folder of your choice. For the purposes of this tutorial, we will be using a predefined repository which we will clone.

`mkdir ~/git && cd git`

Now clone the sample repo.

`git clone https://gitea.kjuulh.io/educrate-articles/article-001.git`

The end result can be found in the folder 'finished', but the place we're going to work is the workspace folder.

`cd article-001/workspace`

## Building the circuit

## How GPIO works

## Building the python program

## Conclusion
