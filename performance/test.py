from locust import  task
from locust.contrib.fasthttp import FastHttpUser


class QuickstartUser(FastHttpUser):
    @task
    def load_page(self):
        self.client.get("/")
